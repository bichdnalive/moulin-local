<footer id="footer">
  <div class="footerInner clearfix">
    <ul class="col1">
      <li><a href="javascript:void(0)" rel="<?php echo APP_URL ?>">トップページ</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a>
        <ul>
          <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
          <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
          <li><a href="<?php echo APP_URL ?>">テキスト<br class="tab">テキスト</a></li>
        </ul>
      </li>
    </ul>
    <ul class="col2">
      <li><a href="<?php echo APP_URL ?>">テキスト<br class="tab">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト<br class="tab">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
    </ul>
    <ul class="col3">
      <li><a href="<?php echo APP_URL ?>">テキスト・<br class="tab">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト・テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li class="SP"><a href="<?php echo APP_URL ?>">テキスト</a></li>
      <li class="SP"><a href="<?php echo APP_URL ?>">テキスト</a></li>
    </ul>
    <div class="col4 PC">
      <p class="text1"><a href="javascript:void(0)" rel="<?php echo APP_URL ?>"><img src="<?php echo APP_ASSETS ?>img/common/logo.svg" width="140" height="30" alt="COMPANY"></a><span>テキストテキストテキスト<br>
テキストテキスト</span></p>
      <p class="text2">〒000-0000<br>
会社の住所</p>
      <p class="text3"><img src="<?php echo APP_ASSETS ?>img/common/icon/ico_phone.svg" alt="" width="13" height="17"><span class="fPhone">0000-000-000</span><span class="fTime">営業時間 0：00〜00：00<br>
定休日 ◯曜日</span></p>
      <p class="text4"><a href="<?php echo APP_URL ?>contact/" class="opa" style="opacity: 1;">Contact us</a></p>
      <ul>
        <li><a href="<?php echo APP_URL ?>">個人情報保護方針</a> | </li>
        <li><a href="<?php echo APP_URL ?>">サイトマップ</a></li>
      </ul>
    </div>
  </div>
  <p class="taC SP"><a href="javascript:void(0)" rel="<?php echo APP_URL ?>"><img src="<?php echo APP_ASSETS ?>img/common/logo.svg" width="157" height="33" alt="COMPANY"></a></p>
  <div class="copyright"><span>Copyright © COMPANY Co. Ltd. <br class="SP">All rights reserved.</span></div>
</footer>

<script src="<?php echo APP_ASSETS; ?>js/common.min.js"></script>